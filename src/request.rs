// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
use crate::config::Account;
use crate::guard::UserGuard;
use anyhow::Result;
use mwbot::SaveOptions;
use rand::seq::SliceRandom;
use serde::Serialize;

#[derive(FromForm, Serialize)]
pub(crate) struct RequestForm {
    id: String,
    lang: String,
    text: String,
    time: Option<String>,
    summary: String,
    comments: Option<String>,
    file: Option<String>,
    file_alt: Option<String>,
}

impl Default for RequestForm {
    fn default() -> Self {
        Self {
            id: rand_id(),
            lang: "".to_string(),
            text: "".to_string(),
            time: None,
            summary: "".to_string(),
            comments: None,
            file: None,
            file_alt: None,
        }
    }
}

pub(crate) async fn handle(
    form: RequestForm,
    account: &Account,
    user: &UserGuard,
) -> Result<()> {
    let bot = account.bot(Some(user)).await?;
    let page = bot.page(&account.page)?;
    let opts = SaveOptions::summary(&form.summary).section("new");
    let new_section = build_template(form);
    page.save(new_section, &opts).await?;
    Ok(())
}

/// Generate a short random ID
fn rand_id() -> String {
    let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".as_bytes();
    let mut rng = rand::thread_rng();
    let selected: Vec<u8> =
        chars.choose_multiple(&mut rng, 5).copied().collect();
    // unwrap: Characters are all valid UTF-8
    String::from_utf8(selected).unwrap()
}

pub(crate) fn armor_text(text: &str) -> String {
    format!("<poem><nowiki>{text}</nowiki></poem>")
}

fn build_template(form: RequestForm) -> String {
    // TODO: allow users to specify a better section heading
    format!(
        r#"{{{{Mastodon post
|id={}
|text={}
|file={}
|file_alt={}
|lang={}
|time={}
|status=pending
}}}}

{}
~~~~
"#,
        rand_id(),
        armor_text(&form.text),
        form.file.unwrap_or_default(),
        form.file_alt.unwrap_or_default(),
        form.lang,
        form.time.unwrap_or_default(),
        form.comments.unwrap_or_default()
    )
}
