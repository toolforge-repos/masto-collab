// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
#[macro_use]
extern crate rocket;

use rocket::form::Form;
use rocket::fs::FileServer;
use rocket::http::{CookieJar, Status};
use rocket::request::FlashMessage;
use rocket::response::{Flash, Redirect};
use rocket::{Request, State};
use rocket_dyn_templates::Template;
use rocket_healthz::Healthz;
use rocket_oauth2::{OAuth2, TokenResponse};
use serde::Serialize;
use std::collections::{BTreeMap, HashMap};

mod api;
mod config;
mod guard;
mod media;
mod parse;
mod request;
mod submit;

use crate::config::{Account, Config};
use crate::guard::AuthorizedUser;
use crate::parse::PostProposal;
use crate::request::RequestForm;
use crate::submit::SubmitForm;
use guard::UserGuard;

const USER_AGENT: &str = toolforge::user_agent!("masto-collab");

#[derive(Serialize)]
struct ErrorTemplate {
    code: u16,
    reason: &'static str,
    error: Option<String>,
}

#[derive(Responder)]
#[response(content_type = "text/html")]
struct Error((Status, Template));

impl From<anyhow::Error> for Error {
    fn from(value: anyhow::Error) -> Self {
        let status = Status::InternalServerError;
        Self((
            status,
            Template::render(
                "error",
                ErrorTemplate {
                    code: status.code,
                    reason: status.reason_lossy(),
                    error: Some(value.to_string()),
                },
            ),
        ))
    }
}

impl From<Status> for Error {
    fn from(value: Status) -> Self {
        Self((
            value,
            Template::render(
                "error",
                ErrorTemplate {
                    code: value.code,
                    reason: value.reason_lossy(),
                    error: None,
                },
            ),
        ))
    }
}

type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Serialize)]
struct IndexTemplate {
    user: Option<UserGuard>,
    accounts: HashMap<String, Account>,
    authorized: BTreeMap<String, usize>,
}

#[get("/")]
async fn index(
    user: Option<UserGuard>,
    cfg: &State<Config>,
) -> Result<Template> {
    let accounts = cfg.accounts.clone();
    let authorized = match &user {
        Some(user) => {
            let mut items = BTreeMap::new();
            for (name, account) in cfg.find_authorized(user) {
                let pending = parse::fetch(&account)
                    .await?
                    .iter()
                    .filter(|post| post.status.is_pending())
                    .count();
                items.insert(name, pending);
            }
            items
        }
        None => BTreeMap::new(),
    };
    Ok(Template::render(
        "index",
        IndexTemplate {
            user,
            accounts,
            authorized,
        },
    ))
}

#[derive(Debug)]
struct MWOAuth;

#[get("/login")]
fn login(oauth2: OAuth2<MWOAuth>, cookies: &CookieJar<'_>) -> Redirect {
    oauth2.get_redirect(cookies, &[]).unwrap()
}

#[get("/auth")]
async fn auth(
    token: TokenResponse<MWOAuth>,
    cookies: &CookieJar<'_>,
) -> Result<Redirect> {
    let username = api::user_info(token.access_token()).await?;
    // Set a login cookie
    cookies.add_private(
        UserGuard {
            username,
            oauth2_token: token.access_token().to_string(),
        }
        .into_cookie(token.expires_in())?,
    );
    Ok(Redirect::to("/"))
}

#[derive(Serialize)]
struct ProposedTemplate {
    user: UserGuard,
    account: Account,
    posts: Vec<PostProposal>,
    is_authorized: bool,
    message: Option<(String, String)>,
}

#[get("/<account>/proposed")]
async fn proposed_get(
    account: String,
    cfg: &State<Config>,
    user: UserGuard,
    flash: Option<FlashMessage<'_>>,
) -> Result<Template> {
    let account = cfg.accounts.get(&account).ok_or(Status::NotFound)?.clone();
    let posts = parse::fetch(&account)
        .await?
        .into_iter()
        .filter(|p| p.status.is_pending())
        .collect();
    let is_authorized = account.authorized.contains(&user.username);
    Ok(Template::render(
        "proposed",
        ProposedTemplate {
            account,
            posts,
            user,
            is_authorized,
            message: flash.map(|flash| {
                (flash.kind().to_string(), flash.message().to_string())
            }),
        },
    ))
}

#[derive(Serialize)]
struct RequestTemplate {
    user: UserGuard,
    account: Account,
    success: bool,
    post: RequestForm,
}

#[get("/<account>/propose")]
async fn request_get(
    account: String,
    cfg: &State<Config>,
    user: UserGuard,
) -> Result<Template> {
    let account = cfg.accounts.get(&account).ok_or(Status::NotFound)?.clone();
    Ok(Template::render(
        "request",
        RequestTemplate {
            user,
            account,
            success: false,
            post: RequestForm::default(),
        },
    ))
}

#[post("/<account>/propose", data = "<form>")]
async fn request_post(
    account: String,
    form: Form<RequestForm>,
    cfg: &State<Config>,
    user: UserGuard,
) -> Result<Template> {
    let account = cfg.accounts.get(&account).ok_or(Status::NotFound)?.clone();
    request::handle(form.into_inner(), &account, &user).await?;
    Ok(Template::render(
        "request",
        RequestTemplate {
            user,
            account,
            success: true,
            post: RequestForm::default(),
        },
    ))
}

#[post("/<account_name>/submit", data = "<form>")]
async fn submit_post(
    account_name: String,
    form: Form<SubmitForm>,
    cfg: &State<Config>,
    user: UserGuard,
) -> Result<Flash<Redirect>> {
    let account = cfg
        .accounts
        .get(&account_name)
        .ok_or(Status::NotFound)?
        .clone();
    let authorized =
        AuthorizedUser::get(user, account).ok_or(Status::Unauthorized)?;
    let message = submit::handle(form.into_inner(), authorized).await?;
    Ok(Flash::success(
        Redirect::to(format!("/{account_name}/proposed")),
        message,
    ))
}

#[catch(default)]
fn default_catcher(status: Status, _req: &Request) -> Error {
    Error::from(status)
}

#[launch]
fn rocket() -> _ {
    let cfg: Config = toml::from_str(
        &std::fs::read_to_string("config.toml").expect("failed to read config"),
    )
    .expect("failed to parse config");
    dbg!(&cfg);
    rocket::build()
        .manage(cfg)
        .attach(Template::fairing())
        .attach(Healthz::fairing())
        .attach(OAuth2::<MWOAuth>::fairing("wikimedia"))
        .mount(
            "/",
            routes![
                index,
                login,
                auth,
                submit_post,
                proposed_get,
                request_get,
                request_post
            ],
        )
        .register("/", catchers![default_catcher])
        .mount("/static", FileServer::from("static"))
}
