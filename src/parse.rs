// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
use crate::config::Account;
use anyhow::Result;
use mwbot::parsoid::{
    ImmutableWikicode, Template, WikiMultinode, WikinodeIterator,
};
use serde::Serialize;

#[derive(Debug, Serialize)]
pub(crate) enum ProposalStatus {
    Pending,
    Posted,
    Rejected,
}

impl ProposalStatus {
    fn parse(input: &str) -> Self {
        match input {
            "posted" => Self::Posted,
            "rejected" => Self::Rejected,
            _ => Self::Pending,
        }
    }

    pub(crate) fn is_pending(&self) -> bool {
        matches!(self, Self::Pending)
    }
}

#[derive(Debug, Serialize)]
pub(crate) struct PostProposal {
    pub(crate) id: String,
    pub(crate) text: String,
    pub(crate) lang: String,
    pub(crate) status: ProposalStatus,
    pub(crate) time: Option<String>,
    pub(crate) file: Option<String>,
    pub(crate) file_alt: Option<String>,
}

pub(crate) async fn fetch(account: &Account) -> Result<Vec<PostProposal>> {
    let bot = account.bot(None).await?;
    let page = bot.page(&account.page)?;
    let html = page.html().await?;
    parse_posts(html)
}

pub(crate) fn id(temp: &Template) -> String {
    if let Some(id) = temp.param("id") {
        return id;
    }
    let node = temp.as_nodes()[0].clone();
    let attribs = node
        .as_element()
        .expect("not an element")
        .attributes
        .borrow();
    attribs.get("id").expect("no id").to_string()
}

fn normalize_text(input: String) -> String {
    let input = match input.strip_prefix("<poem><nowiki>") {
        Some(stripped) => stripped.to_string(),
        None => input,
    };
    let input = match input.strip_suffix("</nowiki></poem>") {
        Some(stripped) => stripped.to_string(),
        None => input,
    };
    input
}

fn parse_posts(html: ImmutableWikicode) -> Result<Vec<PostProposal>> {
    let html = html.into_mutable();
    let mut posts = vec![];
    for template in html.filter_templates()? {
        if template.name() == "Template:Mastodon post" {
            let post = PostProposal {
                id: id(&template),
                text: normalize_text(
                    template.param("text").unwrap_or("".to_string()),
                ),
                lang: template.param("lang").unwrap_or("en".to_string()),
                status: ProposalStatus::parse(
                    template.param("status").unwrap_or("".to_string()).as_str(),
                ),
                time: check_empty(template.param("time")),
                file: check_empty(template.param("file")),
                file_alt: check_empty(template.param("file_alt")),
            };
            posts.push(post);
        }
    }
    Ok(posts)
}

fn check_empty(input: Option<String>) -> Option<String> {
    input.and_then(|val| {
        if val.trim().is_empty() {
            None
        } else {
            Some(val)
        }
    })
}
