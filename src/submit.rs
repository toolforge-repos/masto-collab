// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
use crate::guard::AuthorizedUser;
use crate::parse;
use crate::request::armor_text;
use anyhow::{anyhow, Result};
use mwbot::parsoid::{
    ImmutableWikicode, Template, WikiMultinode, WikinodeIterator,
};
use mwbot::SaveOptions;
use std::collections::HashMap;

#[derive(FromForm)]
pub(crate) struct SubmitForm {
    lang: String,
    id: String,
    text: String,
    action: SubmitAction,
    time: Option<String>,
    comment: String,
    // TODO: should require alt text if file is specified
    file: Option<String>,
    file_alt: Option<String>,
}

#[derive(FromFormField)]
pub(crate) enum SubmitAction {
    #[field(value = "post")]
    Post,
    #[field(value = "reject")]
    Reject,
    #[field(value = "update")]
    UpdateOnWiki,
}

/// Turn empty strings into None
fn normalize_form(mut form: SubmitForm) -> SubmitForm {
    if form.file.as_ref().map(|v| v.is_empty()).unwrap_or(false) {
        form.file = None;
    }
    if form
        .file_alt
        .as_ref()
        .map(|v| v.is_empty())
        .unwrap_or(false)
    {
        form.file_alt = None;
    }
    if form.time.as_ref().map(|v| v.is_empty()).unwrap_or(false) {
        form.time = None;
    }
    form
}

async fn update_wiki(
    form: SubmitForm,
    authorized: &AuthorizedUser,
    status: Option<&str>,
) -> Result<()> {
    let bot = authorized.account().bot(Some(authorized.user())).await?;
    let page = bot.page(&authorized.account().page)?;
    let html = page.html().await?;
    let comment = form.comment.to_string();
    let (html, summary_prefix) = update_template(form, html, status)?;
    page.save(
        html,
        &SaveOptions::summary(&format!("{summary_prefix}{comment}")),
    )
    .await?;
    Ok(())
}

fn update_template(
    form: SubmitForm,
    html: ImmutableWikicode,
    status: Option<&str>,
) -> Result<(ImmutableWikicode, String)> {
    let html = html.into_mutable();
    for temp in html.filter_templates()? {
        if parse::id(&temp) == form.id {
            temp.set_param("id", &form.id)?;
            temp.set_param("text", &armor_text(&form.text))?;
            temp.set_param("lang", &form.lang)?;
            if let Some(status) = status {
                temp.set_param("status", status)?;
                temp.set_param("comment", &format!("{} ~~~~", &form.comment))?;
            }
            temp.set_param("file", form.file.as_deref().unwrap_or_default())?;
            temp.set_param(
                "file_alt",
                form.file_alt.as_deref().unwrap_or_default(),
            )?;
            temp.set_param("time", form.time.as_deref().unwrap_or_default())?;
            let summary_prefix = find_header(&temp);
            return Ok((html.into_immutable(), summary_prefix));
        }
    }
    Err(anyhow!("unable to find template with id={}", form.id))
}

// Find the nearest section header to prepend to the edit summary, or an empty string
// if we can't figure it out
fn find_header(temp: &Template) -> String {
    temp.as_wikinodes()
        .next()
        .unwrap()
        .ancestors()
        .filter_map(|node| node.as_section())
        .filter_map(|section| section.heading())
        .map(|heading| format!("/* {} */ ", heading.text_contents()))
        .next()
        .unwrap_or_default()
}

pub(crate) async fn handle(
    form: SubmitForm,
    authorized: AuthorizedUser,
) -> Result<&'static str> {
    let form = normalize_form(form);
    match form.action {
        SubmitAction::Reject => {
            update_wiki(form, &authorized, Some("rejected")).await?;
            Ok("The post was successfully rejected.")
        }
        SubmitAction::Post => {
            post_to_mastodon(&form, &authorized).await?;
            // TODO: leave link to new post on-wiki?
            update_wiki(form, &authorized, Some("posted")).await?;
            Ok("The post was successfully posted!")
        }
        SubmitAction::UpdateOnWiki => {
            update_wiki(form, &authorized, None).await?;
            Ok("The post was updated on-wiki.")
        }
    }
}

async fn post_to_mastodon(
    form: &SubmitForm,
    authorized: &AuthorizedUser,
) -> Result<()> {
    let mut data: HashMap<&'static str, String> = HashMap::new();
    data.insert("status", form.text.to_string());
    data.insert("visibility", "public".to_string());
    data.insert("language", form.lang.to_string());
    if let Some(time) = &form.time {
        data.insert("scheduled_at", time.to_string());
    }

    if let (Some(file), Some(file_alt)) = (&form.file, &form.file_alt) {
        let media_id = crate::media::upload(
            authorized,
            file.to_string(),
            file_alt.to_string(),
        )
        .await?;
        data.insert("media_ids[]", media_id);
    }

    let url = format!("https://{}/api/v1/statuses", authorized.account().host);
    let client = authorized.account().client()?;
    let resp = client
        .post(url)
        .form(&data)
        .header("Idempotency-Key", &form.id)
        .send()
        .await?
        .error_for_status()?;
    dbg!(&resp);
    Ok(())
}
