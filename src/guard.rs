// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
use crate::config::Account;
use anyhow::{anyhow, Error, Result};
use rocket::form::validate::Contains;
use rocket::http::{Cookie, SameSite, Status};
use rocket::request::{FromRequest, Outcome, Request};
use serde::{Deserialize, Serialize};
use time::Duration;
use tracing::debug;

const COOKIE_NAME: &str = "username";

#[derive(Serialize, Deserialize)]
pub struct UserGuard {
    pub username: String,
    pub oauth2_token: String,
}

impl UserGuard {
    pub(crate) fn into_cookie(
        self,
        expires_in: Option<i64>,
    ) -> Result<Cookie<'static>> {
        Ok(Cookie::build((COOKIE_NAME, serde_json::to_string(&self)?))
            .same_site(SameSite::Lax)
            .max_age(Duration::seconds(expires_in.unwrap_or(60 * 60)))
            .into())
    }
}

/// Type-level proof that the given user is authorized for the given account
pub(crate) struct AuthorizedUser {
    user: UserGuard,
    account: Account,
}

impl AuthorizedUser {
    pub(crate) fn get(
        user: UserGuard,
        account: Account,
    ) -> Option<AuthorizedUser> {
        if account.authorized.contains(&user.username) {
            Some(AuthorizedUser { user, account })
        } else {
            None
        }
    }

    pub(crate) fn user(&self) -> &UserGuard {
        &self.user
    }

    pub(crate) fn account(&self) -> &Account {
        &self.account
    }
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for UserGuard {
    type Error = Error;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let cookie = match req.cookies().get_private(COOKIE_NAME) {
            Some(cookie) => cookie,
            None => {
                return Outcome::Error((
                    Status::Unauthorized,
                    anyhow!("You are not logged in."),
                ))
            }
        };
        match serde_json::from_str(cookie.value()) {
            Ok(guard) => Outcome::Success(guard),
            Err(err) => {
                debug!(
                    "Invalid JSON in username cookie: {} ({})",
                    cookie.value(),
                    err
                );
                Outcome::Error((
                    Status::Unauthorized,
                    anyhow!("You are not logged in."),
                ))
            }
        }
    }
}
