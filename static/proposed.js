$(function() {
    $("select[name=\"action\"]").on("change", function() {
        var $select = $(this);
        var action = $select.val();
        var $comment = $select.parent().parent().find("input[name=\"comment\"]");
        if (["", "Approved", "Sorry, "].includes($comment.val())) {
            if (action == "post") {
                $comment.val("Approved");
            } else if (action == "reject") {
                $comment.val("Sorry, ");
            } else if (action == "update") {
                $comment.val("");
            }
        }
    });

    $("textarea[name=\"text\"]").on("keyup keydown change", function() {
        var $textarea = $(this);
        var $counter = $textarea.parent().find(".text-charcount");
        // TODO: don't hardcode 500, get it from the instance
        $counter.text(500 - $textarea.val().length);
    });

    $("input[name=\"file\"]").on("change", function () {
        var $input = $(this);
        var $alt = $input.parent().parent().find(".file_alt_container");
        var $alt_input = $alt.find("textarea[name=\"file_alt\"]")
        if ($input.val()) {
            $alt.fadeIn();
            $alt_input.prop("required", true);
        } else {
            $alt.fadeOut();
            $alt_input.prop("required", false);
        }
    });
});
