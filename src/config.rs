// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
use crate::guard::UserGuard;
use crate::USER_AGENT;
use anyhow::Result;
use mwbot::Bot;
use reqwest::header;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Deserialize, Debug)]
pub(crate) struct Config {
    pub(crate) accounts: HashMap<String, Account>,
}

impl Config {
    /// Accounts the given user is authorized for
    pub(crate) fn find_authorized(
        &self,
        user: &UserGuard,
    ) -> HashMap<String, Account> {
        self.accounts
            .clone()
            .into_iter()
            .filter(|(_, account)| account.authorized.contains(&user.username))
            .collect()
    }
}

#[derive(Deserialize, Debug, Serialize, Clone)]
pub(crate) struct Account {
    pub(crate) name: String,
    pub(crate) host: String,
    // To avoid leaking, don't ever serialize the token
    #[serde(skip_serializing)]
    pub(crate) token: String,
    pub(crate) wiki: String,
    pub(crate) page: String,
    pub(crate) authorized: Vec<String>,
}

impl Account {
    pub(crate) async fn bot(&self, user: Option<&UserGuard>) -> Result<Bot> {
        let builder = Bot::builder(format!("https://{}/w/", self.wiki))
            .set_user_agent(USER_AGENT.to_string());
        let builder = if let Some(user) = user {
            builder.set_oauth2_token(
                user.username.to_string(),
                user.oauth2_token.to_string(),
            )
        } else {
            builder
        };
        Ok(builder.build().await?)
    }

    pub(crate) fn client(&self) -> Result<reqwest::Client> {
        let mut value =
            header::HeaderValue::from_str(&format!("Bearer {}", self.token))?;
        value.set_sensitive(true);
        let mut headers = header::HeaderMap::new();
        headers.insert(header::AUTHORIZATION, value);
        let client = reqwest::Client::builder()
            .user_agent(USER_AGENT)
            .default_headers(headers)
            .build()?;
        Ok(client)
    }
}
