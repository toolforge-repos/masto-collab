// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
use anyhow::Result;
use mwbot::Bot;

pub async fn user_info(oauth2_token: &str) -> Result<String> {
    let bot = Bot::builder("https://meta.wikimedia.org/w/".to_string())
        .set_oauth2_token("placeholder".to_string(), oauth2_token.to_string())
        .set_user_agent(crate::USER_AGENT.to_string())
        .build()
        .await?;

    let resp = bot
        .api()
        .get_value(&[("action", "query"), ("meta", "userinfo")])
        .await?;
    Ok(resp["query"]["userinfo"]["name"]
        .as_str()
        .unwrap()
        .to_string())
}
