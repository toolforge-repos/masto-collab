// SPDX-License-Identifier: AGPL-3.0-or-later
// Copyright (C) 2023 Kunal Mehta <legoktm@debian.org>
use crate::guard::AuthorizedUser;
use anyhow::{anyhow, Context, Result};
use mwbot::Bot;
use reqwest::multipart;
use serde::Deserialize;
use serde_json::Value;

// Per https://docs.joinmastodon.org/user/posting/#media
const IMAGE_MAX_SIZE: u64 = 16_000_000;
// Mastodon does not natively support SVG, so we must use the PNG thumbs
const MUST_THUMBNAIL: [&str; 1] = ["image/svg+xml"];

#[derive(Deserialize)]
struct ImageInfo {
    size: u64,
    thumburl: String,
    url: String,
    mime: String,
}

async fn commons_bot() -> Result<Bot> {
    Ok(Bot::builder("https://commons.wikimedia.org/w/".to_string())
        .set_user_agent(crate::USER_AGENT.to_string())
        .build()
        .await?)
}

async fn commons_url(bot: &Bot, title: &str) -> Result<(String, String)> {
    let page = bot.page(&format!("File:{title}"))?;
    let mut resp: Value = bot
        .api()
        .get(&[
            ("action", "query"),
            ("prop", "imageinfo"),
            ("titles", page.title()),
            ("iiprop", "size|url|mime"),
            // From <https://docs.joinmastodon.org/user/posting/#media>,
            // "Images will be downscaled to 8.3 megapixels (enough for a 3840x2160 image).""
            ("iiurlwidth", "3840"),
            ("iiurlheight", "2160"),
        ])
        .await?;
    let info: ImageInfo = serde_json::from_value(
        resp["query"]["pages"][0]["imageinfo"][0].take(),
    )?;
    if info.size > IMAGE_MAX_SIZE
        || MUST_THUMBNAIL.contains(&info.mime.as_str())
    {
        // We need to fetch the MIME type from the server, since it might be SVG->PNG
        let resp = bot
            .api()
            .http_client()
            .head(&info.thumburl)
            .send()
            .await?
            .error_for_status()?;
        let mime = resp
            .headers()
            .get("content-type")
            .ok_or_else(|| anyhow!("Missing content-type header"))?
            .to_str()
            .context("Invalid content-type header from thumbnail")?;
        Ok((info.thumburl, mime.to_string()))
    } else {
        Ok((info.url, info.mime))
    }
}

#[derive(Deserialize)]
struct MediaAttachment {
    id: String,
}

pub(crate) async fn upload(
    auth: &AuthorizedUser,
    title: String,
    alt: String,
) -> Result<String> {
    let bot = commons_bot().await?;
    let (image_url, image_mime) = commons_url(&bot, &title).await?;
    // Download the file
    let image_req = bot
        .api()
        .http_client()
        .get(image_url)
        .send()
        .await?
        .error_for_status()?;
    let image_mime = match image_req.headers().get("content-type") {
        Some(header) => header.to_str()?.to_string(),
        None => image_mime,
    };
    let image = image_req.bytes().await?;
    // Send it to Mastodon!
    let api_url = format!("https://{}/api/v2/media", auth.account().host);
    let client = auth.account().client()?;
    let form = multipart::Form::new().text("description", alt).part(
        "file",
        multipart::Part::stream(image)
            .mime_str(&image_mime)?
            .file_name(title),
    );
    let resp = client.post(api_url).multipart(form).send().await?;
    let resp: MediaAttachment = resp.error_for_status()?.json().await?;
    Ok(resp.id)
}

#[cfg(test)]
mod test {
    use super::*;

    #[tokio::test]
    async fn test_commons_url() {
        let bot = commons_bot().await.unwrap();

        // Small PNG, uses original
        let resp = commons_url(&bot, "SMW Con Fall 2023.png").await.unwrap();
        assert_eq!(resp, ("https://upload.wikimedia.org/wikipedia/commons/0/03/SMW_Con_Fall_2023.png".to_string(), "image/png".to_string()));

        // SVG turns into PNG thumbnail (T363314)
        let resp2 =
            commons_url(&bot, "Coolest Tool Award 2024 square logo.svg")
                .await
                .unwrap();
        assert_eq!(resp2, ("https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Coolest_Tool_Award_2024_square_logo.svg/2135px-Coolest_Tool_Award_2024_square_logo.svg.png".to_string(), "image/png".to_string()));

        // 23MB JPG that uses a thumbnail
        let resp3 = commons_url(&bot, "Pillars of Creation (NIRCam Image).jpg")
            .await
            .unwrap();
        assert_eq!(resp3, ("https://upload.wikimedia.org/wikipedia/commons/thumb/f/f1/Pillars_of_Creation_%28NIRCam_Image%29.jpg/1247px-Pillars_of_Creation_%28NIRCam_Image%29.jpg".to_string(), "image/jpeg".to_string()));
    }
}
